﻿using System;
using Input;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class PlayerState : MonoBehaviour
    {
        public static event Action<State> StateChanged;

        [SerializeField] private Image _startGamePanel;
        [SerializeField] private Image _winGamePanel;
        [SerializeField] private Image _loseGamePanel;

        private static PlayerState _instance;
        public static State State => _instance._state;

        private State _state = State.Start;
        private PlayerAnimation _playerAnimation;

        private InputHandler _inputHandler;

        private void Awake() => _instance = this;

        private void OnEnable()
        {
            _playerAnimation = GetComponentInParent<PlayerAnimation>();

            _inputHandler = new InputHandler();
            _inputHandler.FirstTap += OnFirstTap;

            StateChanged?.Invoke(_state);
        }

        private void OnFirstTap() => SetState(State.Play);

        public static void SetState(State state)
        {
            _instance._state = state;
            StateChanged?.Invoke(state);
            _instance.StateChangedHandler();
        }

        private void StateChangedHandler()
        {
            switch (_state)
            {
                case State.Play:
                    _startGamePanel.gameObject.SetActive(false);
                    break;
                case State.Win:
                    _winGamePanel.gameObject.SetActive(true);
                    break;
                case State.Lose:
                    _playerAnimation.PlayFallAnimation();
                    _loseGamePanel.gameObject.SetActive(true);
                    break;
            }
        }

        private void OnDisable() => _inputHandler.FirstTap -= OnFirstTap;
    }

    public enum State
    {
        Start,
        Play,
        Win,
        Lose
    }
}
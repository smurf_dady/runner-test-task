﻿namespace Player.Collisions
{
    public interface ICollision
    {
        void Collide(PlayerAnimation player);
    }
}
﻿using Collisions;
using UnityEngine;

namespace Player.Collisions
{
    public class PlayerCollisions : MonoBehaviour
    {
        private PlayerAnimation _playerAnimation;

        private void Start() => _playerAnimation = GetComponent<PlayerAnimation>();

        private void OnCollisionEnter(Collision other)
        {
            var collision = other.collider.GetComponent<ICollision>();

            if (collision is Obstacle obstacle)
            {
                if (DetectCollisionSide(other))
                    obstacle.Collide(_playerAnimation);
            }
            else
                collision?.Collide(_playerAnimation);
        }

        private bool DetectCollisionSide(Collision other)
        {
            Physics.Raycast(transform.position, other.gameObject.transform.position, out var hit);
            var angle = Vector3.Angle(hit.normal, Vector3.forward);

            return Mathf.Approximately(angle, 180);
        }
    }
}
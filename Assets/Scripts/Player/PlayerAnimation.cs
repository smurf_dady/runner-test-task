﻿using System.Collections;
using Input;
using UnityEngine;

namespace Player
{
    public class PlayerAnimation : MonoBehaviour
    {
        private static readonly int Speed = Animator.StringToHash("Speed");
        private static readonly int MoveToSide = Animator.StringToHash("MoveToSide");
        private static readonly int Jump = Animator.StringToHash("Jump");
        private static readonly int Fall = Animator.StringToHash("Fall");
        private static readonly int Punch = Animator.StringToHash("Punch");

        private Animator _animator;
        private Rigidbody _rigidbody;

        private InputHandler _inputHandler;
        private SwipeDetector _swipeDetector;

        private bool _isGrounded = true;
        private float _height;
        private int _currentLane = 1;

        private void OnEnable()
        {
            _height = GetComponent<Collider>().bounds.size.y;

            _animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody>();

            _inputHandler = new InputHandler();
            _swipeDetector = new SwipeDetector();

            _inputHandler.PlayerMovingInput += OnPlayerMoving;
            _inputHandler.PlayerJumpInput += OnPlayerJump;

            _swipeDetector.Swipe += OnPlayerMoving;
        }

        public void PlayFallAnimation() => _animator.SetTrigger(Fall);

        public void PlayPunchAnimation() => _animator.SetTrigger(Punch);

        private void OnPlayerJump()
        {
            _isGrounded = Physics.Raycast(transform.position, Vector3.down, _height / 2 + 0.1f);
            if (!_isGrounded) return;

            _animator.SetTrigger(Jump);
        }

        private void OnPlayerMoving(int inputValue)
        {
            var targetLane = _currentLane + inputValue;

            if (targetLane < 0 || targetLane > 2) return;
            _currentLane = targetLane;

            _animator.SetInteger(MoveToSide, inputValue);
            StartCoroutine(DisableDodgeAnimation());
        }

        private IEnumerator DisableDodgeAnimation()
        {
            yield return null;
            _animator.SetInteger(MoveToSide, 0);
        }

        private void OnPlayerMoving(float inputValue)
        {
            var targetLane = _currentLane + inputValue;

            if (targetLane < 0 || targetLane > 2) return;
            _currentLane = (int) targetLane;

            _animator.SetInteger(MoveToSide, (int) inputValue);
        }

        private void UpdateMovementAnimation() => _animator.SetFloat(Speed, _rigidbody.velocity.magnitude);

        private void Update() => UpdateMovementAnimation();

        private void OnDisable()
        {
            _inputHandler.PlayerMovingInput -= OnPlayerMoving;
            _inputHandler.PlayerJumpInput -= OnPlayerJump;
            _swipeDetector.Swipe -= OnPlayerMoving;
        }
    }
}
﻿using System.Collections.Generic;
using DG.Tweening;
using Input;
using UnityEngine;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _jumpForce;
        [SerializeField] private float _laneDistance;

        private int _currentLane = 1;
        private float _height;
        private float _startMoveSpeed;
        private bool _isGrounded = true;

        private Dictionary<int, float> _lanePositions = new Dictionary<int, float>();

        private InputHandler _inputHandler;
        private SwipeDetector _swipeDetector;
        private Rigidbody _rigidbody;


        private void OnEnable()
        {
            _height = GetComponent<Collider>().bounds.size.y;

            _startMoveSpeed = _moveSpeed;
            _moveSpeed = 0f;

            _rigidbody = GetComponent<Rigidbody>();

            _inputHandler = new InputHandler();
            _swipeDetector = new SwipeDetector();

            _inputHandler.PlayerMovingInput += MoveToSide;
            _inputHandler.PlayerJumpInput += Jump;

            _swipeDetector.Swipe += MoveToSide;

            PlayerState.StateChanged += OnStateChanged;

            _lanePositions = new Dictionary<int, float>
            {
                {0, -_laneDistance},
                {1, 0f},
                {2, _laneDistance}
            };
        }

        private void Jump()
        {
            _isGrounded = Physics.Raycast(transform.position, Vector3.down, _height / 2 + 0.1f);
            if (!_isGrounded) return;

            _rigidbody.AddForce(Vector3.up * _jumpForce);
        }


        private void OnStateChanged(State state)
        {
            _moveSpeed = state switch
            {
                State.Start => 0f,
                State.Play => _startMoveSpeed,
                State.Win => 0f,
                State.Lose => 0f,
                _ => _moveSpeed
            };
        }

        private void MoveToSide(int inputValue)
        {
            if (inputValue == 0) return;

            var targetLane = _currentLane + inputValue;

            if (targetLane < 0 || targetLane > 2) return;

            _currentLane = targetLane;

            var position = _lanePositions[_currentLane];
            transform.DOMoveX(position, 0.5f);
        }

        private void MoveToSide(float inputValue)
        {
            if (inputValue == 0) return;

            var targetLane = _currentLane + inputValue;

            if (targetLane < 0 || targetLane > 2) return;

            _currentLane = (int) targetLane;

            var position = _lanePositions[_currentLane];
            transform.DOMoveX(position, 0.5f);
        }

        private void FixedUpdate()
        {
            var velocity = _rigidbody.velocity;
            velocity.z = _moveSpeed;
            _rigidbody.velocity = velocity;
        }


        private void OnDisable()
        {
            _inputHandler.PlayerMovingInput -= MoveToSide;
            _inputHandler.PlayerJumpInput -= Jump;
            _swipeDetector.Swipe -= MoveToSide;
            PlayerState.StateChanged -= OnStateChanged;
        }
    }
}
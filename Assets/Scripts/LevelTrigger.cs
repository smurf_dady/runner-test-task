﻿using Player;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelTrigger : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] _particles;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerMovement>();
        if (player == null) return;

        PlayerState.SetState(State.Win);

        foreach (var particle in _particles)
        {
            particle.Play();
        }
    }

    public void RestartLevel() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
}
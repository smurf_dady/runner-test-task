using System;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Input
{
    public class InputHandler
    {
        public event Action FirstTap;

        public event Action<Vector2, float> StartTouch;
        public event Action<Vector2, float> EndTouch;

        public event Action<float> PlayerMovingInput;
        public event Action PlayerJumpInput;

        private readonly PlayerControls _playerControls;

        public InputHandler()
        {
            _playerControls = new PlayerControls();
            _playerControls.Enable();

            _playerControls.Player.Movement.performed += OnMovementInputPerfomed;
            _playerControls.Player.Jump.started += OnJumpInputStarted;

            _playerControls.Player.PrimaryContact.started += StartTouchHandler;
            _playerControls.Player.PrimaryContact.canceled += EndTouchHandler;

            _playerControls.Player.FIrstTap.started += context => FirstTap?.Invoke();
        }

        private void StartTouchHandler(InputAction.CallbackContext context)
        {
            if (PlayerState.State != State.Play) return;
            StartTouch?.Invoke(
                Camera.main.ScreenToWorld(_playerControls.Player.PrimaryPosition.ReadValue<Vector2>()),
                (float) context.startTime);
        }

        private void EndTouchHandler(InputAction.CallbackContext context)
        {
            if (PlayerState.State != State.Play) return;
            EndTouch?.Invoke(
                Camera.main.ScreenToWorld(_playerControls.Player.PrimaryPosition.ReadValue<Vector2>()),
                (float) context.time);
        }

        private void OnMovementInputPerfomed(InputAction.CallbackContext context)
        {
            if (PlayerState.State != State.Play) return;
            PlayerMovingInput?.Invoke(_playerControls.Player.Movement.ReadValue<float>());
        }

        private void OnJumpInputStarted(InputAction.CallbackContext context)
        {
            if (PlayerState.State != State.Play) return;
            PlayerJumpInput?.Invoke();
        }
    }
}
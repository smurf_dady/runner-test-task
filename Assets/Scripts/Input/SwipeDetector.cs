﻿using System;
using UnityEngine;

namespace Input
{
    public class SwipeDetector
    {
        public event Action<int> Swipe;

        private readonly float _minimumDistance = 0.1f;
        private readonly float _maximumTimer = 1f;

        private Vector2 _startPosition;
        private Vector2 _endPosition;
        private float _startTime;
        private float _endTime;


        public SwipeDetector()
        {
            var inputHandler = new InputHandler();

            inputHandler.StartTouch += OnStartTouch;
            inputHandler.EndTouch += OnEndTouch;
        }

        private void OnStartTouch(Vector2 position, float time)
        {
            _startPosition = position;
            _startTime = time;
        }

        private void OnEndTouch(Vector2 position, float time)
        {
            _endPosition = position;
            _endTime = time;

            DetectSwipe();
        }

        private void DetectSwipe()
        {
            if (Vector3.Distance(_startPosition, _endPosition) >= _minimumDistance &&
                _endTime - _startTime <= _maximumTimer)
            {
                var direction = (_endPosition - _startPosition).normalized;
                CheckSwipeDirection(direction);
            }
        }

        private void CheckSwipeDirection(Vector2 direction)
        {
            if (Vector2.Dot(Vector3.left, direction) > 0.2f)
                Swipe?.Invoke(-1);
            else if (Vector2.Dot(Vector2.right, direction) > 0.2f)
                Swipe?.Invoke(1);
        }
    }
}
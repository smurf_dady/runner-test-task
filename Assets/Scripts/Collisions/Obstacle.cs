﻿using Player;
using Player.Collisions;
using UnityEngine;

namespace Collisions
{
    public class Obstacle : MonoBehaviour, ICollision
    {
        public void Collide(PlayerAnimation player) => PlayerState.SetState(State.Lose);
    }
}
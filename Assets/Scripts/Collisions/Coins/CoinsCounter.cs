﻿using System;
using System.Collections.Generic;
using Player;
using UnityEngine;

namespace Collisions.Coins
{
    public class CoinsCounter : MonoBehaviour
    {
        public event Action<int> CoinAdded;

        private readonly List<Coin> _coins = new List<Coin>();

        private void Awake() => CoinAdded?.Invoke(_coins.Count);

        private void OnEnable() => PlayerState.StateChanged += OnStateChanged;

        private void OnStateChanged(State state)
        {
            if (state != State.Win) return;

            CoinAdded?.Invoke(_coins.Count);
        }

        public void AddCoins(Coin coin)
        {
            _coins.Add(coin);
            CoinAdded?.Invoke(_coins.Count);
        }

        private void OnDisable() => PlayerState.StateChanged -= OnStateChanged;
    }
}
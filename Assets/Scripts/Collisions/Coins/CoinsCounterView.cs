﻿using TMPro;
using UnityEngine;

namespace Collisions.Coins
{
    public class CoinsCounterView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _coinsCountText;
        [SerializeField] private TextMeshProUGUI _globalCountText;
        private CoinsCounter _coinsCounter;

        private void OnEnable()
        {
            _coinsCounter = GetComponent<CoinsCounter>();
            _coinsCounter.CoinAdded += OnCoinAdded;
        }

        private void OnCoinAdded(int count)
        {
            _coinsCountText.text = count.ToString();
            _globalCountText.text = count.ToString();
        }

        private void OnDisable() => _coinsCounter.CoinAdded -= OnCoinAdded;
    }
}
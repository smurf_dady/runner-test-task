﻿using System;
using Player;
using Player.Collisions;
using UnityEngine;

namespace Collisions.Coins
{
    public class Coin : MonoBehaviour, ICollision
    {
        [SerializeField] private float _rotationSpeed;
        private CoinsCounter _coinsCounter;

        private void Start() => _coinsCounter = FindObjectOfType<CoinsCounter>();

        private void Update() => transform.Rotate(Vector3.forward * (_rotationSpeed * Time.deltaTime));

        public void Collide(PlayerAnimation player)
        {
            _coinsCounter.AddCoins(this);
            Destroy(gameObject);
        }
    }
}
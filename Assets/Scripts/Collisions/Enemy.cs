﻿using System;
using Player;
using Player.Collisions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Collisions
{
    public class Enemy : MonoBehaviour, ICollision
    {
        [SerializeField] private float _forcePower;
        [SerializeField] private int _currentLane;

        private static readonly int GetHit = Animator.StringToHash("GetHit");
        private Rigidbody _rigidbody;
        private Animator _animator;

        private Vector3 _forceDirection;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _animator = GetComponent<Animator>();
        }

        public void Collide(PlayerAnimation player)
        {
            player.PlayPunchAnimation();
            _animator.SetTrigger(GetHit);

            _forceDirection = _currentLane switch
            {
                0 => new Vector3(-1f, 0f, 1f),
                1 => Convert.ToBoolean(Random.Range(-1, 1)) ? new Vector3(-1f, 0f, 1f) : new Vector3(1f, 0f, 1f),
                2 => new Vector3(1f, 0f, 1f),
                _ => _forceDirection
            };

            _rigidbody.AddForce(_forceDirection * _forcePower, ForceMode.Impulse);

            Destroy(gameObject, 5f);
        }
    }
}
﻿using UnityEngine;

public static class Utilities
{
    public static Vector3 ScreenToWorld(this Camera camera, Vector3 position)
    {
        position.z = camera.nearClipPlane;
        return camera.ScreenToWorldPoint(position);
    }
}
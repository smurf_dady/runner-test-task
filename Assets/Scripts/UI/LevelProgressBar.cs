﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LevelProgressBar : MonoBehaviour
    {
        [SerializeField] private Image _fillBar;

        [SerializeField] private Transform _player;
        [SerializeField] private Transform _levelEnd;

        private float _distance;

        private void Start() => _distance = GetDistance();

        private void Update() => UpdateProgressBar();

        private float GetDistance() => (_levelEnd.transform.position - _player.transform.position).sqrMagnitude;

        private void UpdateProgressBar()
        {
            if (_player.transform.position.z > _levelEnd.transform.position.z) return;

            var newDistance = GetDistance();
            var progressValue = Mathf.InverseLerp(_distance, 0f, newDistance);

            _fillBar.fillAmount = progressValue;
        }
    }
}